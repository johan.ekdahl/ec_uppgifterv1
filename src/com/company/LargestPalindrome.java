package com.company;

import java.util.Objects;
import java.util.Random;
import java.util.Stack;

public class LargestPalindrome {


    public static void main(String[] args) {


        //
        //https://projecteuler.net/problem=4 <--- Quiz
        //


        int palindrome = 0;
        int x = 0;
        int y = 0;

        for(int i = 100; i<1000; i++){
            for(int j = 100; j<1000; j++){
                if(isPalindrome(i,j)){
                    palindrome = i * j;
                    x = i;
                    y = j;
                }
            }
        }

        System.out.println("Högsta Palindromet: " + palindrome + " Faktor1: " + x + " Faktor2: " + y);

    }
    //Me thinking
    // 9 0 0 9 / 5.length 5-3 = 2
    public static boolean isPalindrome(int num1, int num2){
        int total = num1 * num2;
        String pally = String.valueOf(total);
        String[] pallarray = pally.split("");
        boolean bl = true;
        for(int i=0; i < pallarray.length; i++){
            if (!Objects.equals(pallarray[i], pallarray[pallarray.length - (i + 1)])) {
                bl = false;
                break;
            }
        }

        return bl;
    }

}
